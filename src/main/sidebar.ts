import { Widget } from "prest-lib/dist/hsml-widget";
import { Hsmls, Hsml } from "prest-lib/dist/hsml";

interface Menu {
    hash: string;
    label: string;
    icon: string;
}

// interface User {
//     name: string;
//     avatar: string;
// }

export class SidebarWidget extends Widget {

    private _hash = "";
    // private _user: User;
    private _isLoggedIn = false;
    private _onClick: () => void;
    private _commonRoutes: Menu[] = [
        { hash: "about", label: "About", icon: "i.fa.fa-info-circle.fa-fw" },
        { hash: "tutorial", label: "Tutorial", icon: "i.fa.fa-university.fa-fw" }
    ];

    private _publicRoutes: Menu[] = [
        { hash: "auth", label: "Login", icon: "i.fa.fa-plus-circle.fa-fw" },
        ...this._commonRoutes
    ];
    private _privateRoutes: Menu[] = [
        // { hash: "account", label: "Account", icon: "i.fa.fa-users.fa-fw" },
        { hash: "add-record", label: "Add record", icon: "i.fa.fa-plus-circle.fa-fw" },
        { hash: "view-records", label: "View records", icon: "i.fa.fa-list.fa-fw" },
        ...this._commonRoutes
    ];
    private _routes: Menu[] = [];
    // private _settings: Menu[] = [
    //     { hash: "settings-acc", label: "Account", icon: "i.fa.fa-cog.fa-fw" },
    //     { hash: "settings-bc", label: "Blockchain", icon: "i.fa.fa-cogs.fa-fw" }
    // ];
    // private _info: Menu[] = [
    //     { hash: "news", label: "News", icon: "i.fa.fa-bell.fa-fw" }
    // ];
    private _nbsp = "\u00a0 ";

    constructor() {
        super("SidebarWidget");
    }

    // setUser(user: User): this {
    //     this._user = user;
    //     this.update();
    //     return this;
    // }

    setHash(hash: string): this {
        this._hash = hash;
        this.update();
        return this;
    }

    onMount() {
        // console.log("onMount", this.type, this.id);
    }

    onUmount() {
        // console.log("onUmount", this.type, this.id);
    }

    setLoggedIn(isLoggedIn: boolean): this {
        this._isLoggedIn = isLoggedIn;
        this._routes = isLoggedIn ? this._privateRoutes : this._publicRoutes;
        return this;
    }

    setOnClick = (handler: () => void): this => {
        this._onClick = handler;
        return this;
    }

    onClick = (): void => {
        this._onClick && this._onClick();
    }

    render(): Hsmls {
        return [
            ["nav", [
                // ["br"],
                // ["div.w3-container.w3-row",
                //     ["div.w3-col.s4",
                //         ["img.w3-circle.w3-margin-right",
                //             {
                //                 src:  this._user.avatar || "https://www.w3schools.com/w3images/avatar2.png",
                //                 style: "width:46px"
                //             }
                //         ]
                //     ],
                //     ["div.w3-col.s8.w3-bar",
                //         ["span",
                //             ["a", { href: "#", style: "text-decoration: none;" }, "Welcome"],
                //             this._user.name ? ", " : " ",
                //             ["strong~name", this._user.name]
                //         ],
                //         ["br"],
                //         ["a.w3-bar-item.w3-button", { href: "#messages", title: "Messages" },
                //             ["i.fa.fa-envelope"]
                //         ],
                //         ["a.w3-bar-item.w3-button", { href: "#profile", title: "Profile" },
                //             ["i.fa.fa-user"]
                //         ],
                //         ["a.w3-bar-item.w3-button", { href: "#settings", title: "Settings" },
                //             ["i.fa.fa-cog"]
                //         ],
                //     ],
                // ],
                // ["hr"],
                // ["div.w3-container", [
                //     ["h5", ["Account"]],
                // ]],
                ["div.w3-bar-block.w3-margin-top", [
                    ...this._routes.map<Hsml>(m => {
                        return (
                            ["a.w3-bar-item.w3-button.w3-padding",
                                {
                                    href: `#${m.hash}`,
                                    classes: m.hash === this._hash ? ["w3-blue"] : []
                                },
                                [[m.icon], this._nbsp, m.label]
                            ]);
                    }),
                    this._isLoggedIn ?
                        ["button.w3-bar-item.w3-button.w3-padding",
                            { click: this.onClick },
                            [
                                ["i.fa.fa-sign-out.fa-fw"] as Hsml,
                                this._nbsp,
                                "Log out"
                            ]
                        ] :
                        " ",
                ]],
                // ["hr"],
                // ["div.w3-container",
                //     ["h5", "Settings"],
                // ],
                // ["div.w3-bar-block",
                //     ...this._settings.map(m => {
                //         return (
                //             ["a.w3-bar-item.w3-button.w3-padding",
                //                 {
                //                     href: `#${m.hash}`,
                //                     classes: m.hash === this._hash ? ["w3-blue"] : []
                //                 },
                //                 [m.icon], this._nbsp, m.label
                //             ]);
                //     }),
                // ],
                // ["hr"],
                // ["div.w3-container",
                //     ["h5", "Info"],
                // ],
                // ["div.w3-bar-block",
                //     ...this._info.map(m => {
                //         return (
                //             ["a.w3-bar-item.w3-button.w3-padding",
                //                 {
                //                     href: `#${m.hash}`,
                //                     classes: m.hash === this._hash ? ["w3-blue"] : []
                //                 },
                //                 [m.icon], this._nbsp, m.label
                //             ]);
                //     }),
                // ],
                ["br"],
                ["br"]
            ]]
        ];
    }

}

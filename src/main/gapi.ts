import { script } from "prest-lib/dist/load";

declare global {
    interface Window {
        gapi: any;
    }
}

interface DriveFile {
    id: string;
    name: string;
}

export class GapiHandler {
    private readonly _filename: string;

    private readonly _CLIENT_ID: string;
    private readonly _API_KEY: string;
    private readonly _DISCOVERY_DOCS: string[];
    private readonly _SCOPES: string;

    private readonly _range: string;
    private _spreadSheetId?: string;

    private _authStatusListener: (isSignedIn: boolean) => void;

    constructor() {
        this._filename = "Dia Sheet";

        this._CLIENT_ID = "744945888533-kjab34vhefm83gss0jffk92vtd8ru2oa.apps.googleusercontent.com";
        this._API_KEY = "AIzaSyAdZWZqWkqQr0ibMrQMZv6fUuGESp0Fd4A";
        this._DISCOVERY_DOCS = [
            "https://sheets.googleapis.com/$discovery/rest?version=v4",
            "https://www.googleapis.com/discovery/v1/apis/drive/v3/rest"
        ];
        this._SCOPES = "https://www.googleapis.com/auth/spreadsheets https://www.googleapis.com/auth/drive.metadata.readonly";

        this._spreadSheetId = null;
        this._range = "Sheet1!A1:D";
    }

    init = (): Promise<any> => {
        return new Promise((resolve, reject) => {
            script("https://apis.google.com/js/api.js", () => {
                window.gapi.load("client:auth2", () => {
                    window.gapi.client.init({
                        apiKey: this._API_KEY,
                        clientId: this._CLIENT_ID,
                        discoveryDocs: this._DISCOVERY_DOCS,
                        scope: this._SCOPES
                    })
                    .then(() => resolve())
                    .catch(() => reject());
                });
            });
        });
    }

    signIn(): void {
        window.gapi.auth2.getAuthInstance().signIn();
    }

    signOut(): void {
        window.gapi.auth2.getAuthInstance().signOut();
    }

    createSheet = (): Promise<any> => {
        return window.gapi.client.sheets.spreadsheets.create({
            properties: {
                title: this._filename
            }
        });
    }

    getSheet = (): Promise<any> => {
        return window.gapi.client.sheets.spreadsheets.values.get(
            {
                spreadsheetId: this._spreadSheetId,
                range: this._range,
            });
    }

    appendToSheet = (data: any): Promise<any> => {
        return window.gapi.client.sheets.spreadsheets.values.append({
            spreadsheetId: this._spreadSheetId,
            range: this._range,
            valueInputOption: "USER_ENTERED",
            insertDataOption: "INSERT_ROWS",
            resource: {
                values: [data]
            }
        });
    }

    getSpreadSheetId = (): string => {
        return this._spreadSheetId;
    }

    setSpreadSheetId = (id: string): this => {
        this._spreadSheetId = id;
        return this;
    }

    setAuthStatusListener = (handler: (isSignedIn: boolean) => void): void => {
        window.gapi.auth2.getAuthInstance().isSignedIn.listen(handler);
        this._authStatusListener = handler;
    }

    getAuthStatusListener = (): (isSignedIn: boolean) => void => {
        return this._authStatusListener;
    }

    getAuthStatus = (): boolean => {
        return window.gapi.auth2.getAuthInstance().isSignedIn.get();
    }

    checkIfSheetExists = (): Promise<DriveFile | null> => {
        return new Promise((resolve, reject) => {
            window.gapi.client.drive.files.list({
                "fields": "nextPageToken, files(id, name)"
            })
            .then((response: any) => {
                if (response.result && response.result.files) {
                    const sheet = (response.result.files as DriveFile[])
                        .filter((item) => item.name === this._filename)[0];
                    resolve(sheet);
                }
            })
            .catch(() => reject());
        });
    }
}

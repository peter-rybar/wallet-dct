import { Widget } from "prest-lib/dist/hsml-widget";
import { Hsmls, Hsml } from "prest-lib/dist/hsml";

interface State {
    data: string[][];
}

export class ViewRecordsWidget extends Widget {
    private _state: State;
    private _onClick: () => void;
    private _sheetUrl: string;

    constructor() {
        super("ViewRecordsWidget");

        this._state = { data: [] };
    }

    onMount() {
        // console.log("onMount", this.type, this.id);
    }

    onUmount() {
        // console.log("onUmount", this.type, this.id);
    }

    getState = (): State => {
        return this._state;
    }

    setState = (state: Partial<State>): void => {
        this._state = { ...this._state, ...state };
        this.update();
    }

    setSheetUrl = (id: string): this => {
        this._sheetUrl = `https://docs.google.com/spreadsheets/d/${id}`;
        return this;
    }

    render(): Hsmls {
        return [
            ["h2", ["View all records"]],
            ["div",
                [
                    ["p.w3-left", [
                        ["button.w3-btn.w3-green",
                            { click: this.onClick },
                            ["Add new record"]
                        ]
                    ]],
                    ["p.w3-right", [
                        ["a", { href: this._sheetUrl, target: "_blank" }, ["View on Drive"]]
                    ]]
                ]
            ],
            ...this._renderSheet()
        ];
    }

    setOnClick = (handler: () => void): this => {
        this._onClick = handler;
        return this;
    }

    onClick = () => {
        this._onClick && this._onClick();
    }

    private _renderSheet = (): Hsmls => {
        const data = this._state.data.length > 0
            ? this._state.data.map<Hsml>(row =>
                ["tr", row.map<Hsml>(cell =>
                    ["td", [cell]]
                )]
            )
            : [];

        return [
            ["div#sheet.w3-margin-top", [
                ["table.w3-table-all", [
                    ["tr", [
                        ["th", ["Date"]],
                        ["th", ["Time"]],
                        ["th", ["Blood sugar"]],
                        ["th", ["Insulin Dose"]]
                    ]],
                    ...data
                ]]
            ]]
        ];
    }
}

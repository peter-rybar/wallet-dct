import { Widget } from "prest-lib/dist/hsml-widget";
import { Hsmls } from "prest-lib/dist/hsml";
import * as moment from "moment";

interface FormData {
    date: string;
    time: string;
    sugarLevel: string;
    insulinDose: string;
}

export class AddRecordWidget extends Widget {

    private readonly _form: FormData;
    private readonly _date: Date;
    private _onSave: (data: string[]) => void;
    private _isLoading: boolean;

    constructor() {
        super("AddRecordWidget");
        this._date = new Date();
        this._isLoading = false;

        this._form = {
            date: moment(this._date).format(moment.HTML5_FMT.DATE),
            time: moment(this._date).format(moment.HTML5_FMT.TIME),
            insulinDose: "0",
            sugarLevel: "0"
        };
    }

    onMount() {
        // console.log("onMount", this.type, this.id);
    }

    onUmount() {
        // console.log("onUmount", this.type, this.id);
    }

    onSave(onSave: (data: string[]) => void): this {
        this._onSave = onSave;
        return this;
    }

    resetForm = (): this => {
        (this.refs["sugarLevel"] as HTMLInputElement).value = "0";
        (this.refs["insulinDose"] as HTMLInputElement).value = "0";
        return this;
    }

    setIsLoading = (isLoading: boolean): this => {
        this._isLoading = isLoading;
        this.update();
        return this;
    }

    render(): Hsmls {
        return [
            ["h2", ["Add a new record"]],
            ["div", {}, [
                ["form", { submit: this._onSubmit }, [
                    ["div.w3-row", {}, [
                        ["p.w3-half.input-left", [
                            ["label", [
                                "Date ",
                                ["input.w3-input~date",
                                    {
                                        type: "date",
                                        value: this._form.date,
                                        required: true
                                    }
                                ]
                            ]],
                            " "
                        ]],
                        ["p.w3-half.input-right", [
                            ["label", [
                                "Time ",
                                ["input.w3-input~time",
                                    {
                                        type: "time",
                                        value: this._form.time,
                                        required: true
                                    }
                                ]
                            ]],
                            " "
                        ]]
                    ]],
                    ["div.w3-row", [
                        ["p.w3-half.input-left", [
                            ["label", [
                                "Blood sugar ",
                                ["input.w3-input~sugarLevel",
                                    {
                                        type: "number",
                                        value: this._form.sugarLevel,
                                        step: .1,
                                        min: 0,
                                        required: true
                                    }
                                ]
                            ]], " "
                        ]],
                        ["p.w3-half.input-right", [
                            ["label", [
                                "Insulin dose ",
                                ["input.w3-input~insulinDose",
                                    {
                                        type: "number",
                                        value: this._form.insulinDose,
                                        step: .1,
                                        min: 0,
                                        required: true
                                    }
                                ]
                            ]],
                            " "
                        ]],
                    ]],
                    ["p", [
                        this._isLoading ?
                            ["button.w3-btn.w3-blue",
                                { disabled: true },
                                [["span.loader"]]
                            ]
                            :
                            ["button.w3-btn.w3-blue", ["Save"]]
                    ]]
                ]]
            ]]
        ];
    }

    private _onSubmit = (e: Event): void => {
        e.preventDefault();

        const data = [
            (this.refs["date"] as HTMLInputElement).value,
            (this.refs["time"] as HTMLInputElement).value,
            (this.refs["sugarLevel"] as HTMLInputElement).value,
            (this.refs["insulinDose"] as HTMLInputElement).value
        ];

        this._onSave && this._onSave(data);
    }
}

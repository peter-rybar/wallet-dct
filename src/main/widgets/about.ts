import { Widget } from "prest-lib/dist/hsml-widget";
import { Hsmls } from "prest-lib/dist/hsml";

export class AboutWidget extends Widget {
    constructor() {
        super("AboutWidget");
    }

    onMount() {
        // console.log("onMount", this.type, this.id);
    }

    onUmount() {
        // console.log("onUmount", this.type, this.id);
    }

    render(): Hsmls {
        return [
            ["h2", ["Diabetes sheet"]],
            ["p", [
                "Web/Mobile Diabetes sheet application (PWA)"
            ]],
            ["h3", ["What it is"]],
            ["div.w3-card", [
                ["img.image",
                    {
                        src:  "assets/images/img.png"
                    }
                ]
            ]],
            ["p", [
                "Diabetes sheet is a Blood Sugar Diary/Spreadsheet Progressive Web App which writes diabetic data to Google sheet for better data visualization and online sharing with your health care provider, supervising doctor, parents or friends."
            ]],
            ["h3", ["Features"]],
            ["div.w3-row.w3-stretch", [
                ["div.w3-col.m6.l3.w3-panel", [
                    ["div.w3-card-4.w3-white", [
                        ["div.w3-padding", [
                            ["img.image",
                                {
                                    src:  "assets/images/padlock.png",
                                    alt: "padlock"
                                }
                            ]
                        ]],
                        ["div.w3-container", [
                            ["h4.feature-title", ["Privacy oriented"]],
                            ["p", [
                                "DiaSheet takes privacy seriously. Your data is stored on your Google Drive for improved security and privacy and is never stored on our servers. You are ultimately in charge of your privacy."
                            ]]
                        ]]
                    ]]
                ]],
                ["div.w3-col.m6.l3.w3-panel", [
                    ["div.w3-card-4.w3-white", [
                        ["div.w3-padding", [
                            ["img.image",
                                {
                                    src:  "assets/images/doctor.png",
                                    alt: "doctor"
                                }
                            ]
                        ]],
                        ["div.w3-container", [
                            ["h4.feature-title", ["Easily shareable"]],
                            ["p", [
                                "DiaSheet makes it easy for you to share your data with your health care provider, supervising doctor, family and friends, allowing for better control and improved health."
                            ]]
                        ]]
                    ]]
                ]],
                ["div.w3-col.m6.l3.w3-panel", [
                    ["div.w3-card-4.w3-white", [
                        ["div.w3-padding", [
                            ["img.image",
                                {
                                    src:  "assets/images/family.png",
                                    alt: "family"
                                }
                            ]
                        ]],
                        ["div.w3-container", [
                            ["h4.feature-title", ["Parental control"]],
                            ["p", [
                                "DiaSheet makes parental control easy. You can track your child's progress, identify weak spots and help them better manage their diabetes."
                            ]]
                        ]]
                    ]]
                ]],
                ["div.w3-col.m6.l3.w3-panel", [
                    ["div.w3-card-4.w3-white", [
                        ["div.w3-padding", [
                            ["img.image",
                                {
                                    src:  "assets/images/responsive.png",
                                    alt: "cross platform"
                                }
                            ]
                        ]],
                        ["div.w3-container", [
                            ["h4.feature-title", ["Cross platform"]],
                            ["p", [
                                "DiaSheet is a Progressive Web App and can be used on any device, whether mobile or desktop. You can use it in your browser or install it directly to your home screen and take your data with you everywhere."
                            ]]
                        ]]
                    ]]
                ]]
            ]],
            ["footer", [
                ["p.w3-small", [
                    ["span", ["Icons made by "]],
                    ["a",
                        {
                            href: "https://www.freepik.com/",
                            target: "_blank",
                            title: "Freepik"
                        },
                        ["Freepik"]
                    ],
                    ["span", [" from "]],
                    ["a",
                        {
                            href: "https://www.flaticon.com/",
                            target: "_blank",
                            title: "Flaticon"
                        },
                        ["Flaticon"]
                    ],
                    ["span", [" licensed by "]],
                    ["a",
                        {
                            href: "http://creativecommons.org/licenses/by/3.0/",
                            target: "_blank",
                            title: "CC 3.0 BY"
                        },
                        ["CC 3.0 BY"]
                    ],
                ]]
            ]]
        ];
    }
}
